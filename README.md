SaSEp: Clairvoyance
---------------------

## Installation guide

### Prerequisites

- Postgres database (You can use our preconfigured test database as defined in application.properties)
- Keycloak (our instance is preconfigured)
- NodeJS
- Maven
- Java JDk
- Git

### Installation

#### Backend
- Git clone the backend repository: ```git@gitlab.com:sasep-clairvoyance/sasep-backend.git```
- CD into the directory.
- Change server port to ```9000``` in /resources/application.properties.
- Run ```mvn package```
- Run ```java -jar target/swagger-spring-1.0.0.jar```

#### Frontend
- Git clone the frontend repository: ```git@gitlab.com:sasep-clairvoyance/sasep-frontend.git```
- CD into the directory.
- Change the environment variable VUE_APP_SASEP_API_URL to the backend URL ```http://localhost:9000``` in .env.development
- Run ```npm install```
- Run ```npm run serve```
- Access the frontend with: ```localhost:8080```

### Done. You have a running Clairvoyance instance.

### Test Coverage

[![coverage report](https://gitlab.com/sasep-clairvoyance/sasep-backend/badges/develop/coverage.svg)](https://gitlab.com/sasep-clairvoyance/sasep-backend/-/commits/develop)

### Metrics: SonarQube

[![Quality Gate Status](https://sonarqube.mysticrunes.net/api/project_badges/measure?project=sasep-backend&metric=alert_status)](https://sonarqube.mysticrunes.net/dashboard?id=sasep-backend)

### Pipeline report:

[![pipeline status](https://gitlab.com/sasep-clairvoyance/sasep-backend/badges/develop/pipeline.svg)](https://gitlab.com/sasep-clairvoyance/sasep-backend/-/commits/develop)
