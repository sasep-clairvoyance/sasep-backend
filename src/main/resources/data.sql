insert into sport (id, name, thumbnail_url, back_color, image_url)
values (01, 'League of Legends', '/logos/sports/LoLthumb.png', 'blue', '/logos/sports/LoLlogo.png');
insert into sport (id, name, thumbnail_url, back_color, image_url)
values (02, 'Dota2', '/logos/sports/Dota2thumb.png', 'blue', '/logos/sports/Dota2logo.png');
insert into sport (id, name, thumbnail_url, back_color, image_url)
values (03, 'CS:GO', '/logos/sports/CSthumb.png', 'blue', '/logos/sports/CSlogo.png');
insert into sport (id, name, thumbnail_url, back_color, image_url)
values (04, 'Overwatch', '/logos/sports/OWthumb.png', 'blue', '/logos/sports/OWlogo.png');
insert into league (id, name, logo, privacy, owner, sport_id)
values (010, 'League Championship Series', '/logos/leagues/LCSlogo.png', false, null, 01);
insert into league (id, name, logo, privacy, owner, sport_id)
values (011, 'European Masters', '/logos/leagues/EUMlogo.png', true, 'Mikkel', 01);
insert into league (id, name, logo, privacy, owner, sport_id)
values (012, 'League of Legends European Championship', '/logos/leagues/LEClogo.png', false, null, 01);
insert into league (id, name, logo, privacy, owner, sport_id)
values (013, 'League of Legends Champions Korea', '/logos/leagues/LCKlogo.png', false, null, 01);
insert into league (id, name, logo, privacy, owner, sport_id)
values (014, 'League of Legends Pro League', '/logos/leagues/LPLlogo.png', false, null, 01);
insert into league (id, name, logo, privacy, owner, sport_id)
values (020, 'Movistar Liga Pro Gaming Season 5', '/logos/leagues/LPGlogo.png', false, 'Michael', 02);
insert into league (id, name, logo, privacy, owner, sport_id)
values (021, 'Parimatch League Season 3', '/logos/leagues/PLSlogo.png', false, 'Michael', 02);
insert into team (id, name, logo, elo, league_id)
values (0100, 'Cloud9', '/logos/Cloud9logo.png', 2644, 010);
insert into team (id, name, logo, elo, league_id)
values (0101, 'Evil Geniuses', '/logos/EGlogo.png', 2514, 010);
insert into team (id, name, logo, elo, league_id)
values (0102, '100 Thieves', '/logos/100Thieveslogo.png', 2412, 010);
insert into team (id, name, logo, elo, league_id)
values (0103, 'FlyQuest', '/logos/FlyQuestlogo.png', 2410, 010);
insert into team (id, name, logo, elo, league_id)
values (0104, 'TSM', '/logos/TSMlogo.png', 2413, 010);
insert into team (id, name, logo, elo, league_id)
values (0105, 'Counter Logic Gaming', '/logos/CLGlogo.png', 2798, 010);
insert into team (id, name, logo, elo, league_id)
values (0106, 'Team Liquid', '/logos/TLlogo.png', 2133, 010);
insert into team (id, name, logo, elo, league_id)
values (0107, 'Golden Guardians', '/logos/GGlogo.png', 2599, 010);
insert into match (id, date, location, team1_id, team2_id, winner_id)
values (01000, '2020-05-12', 'Atlanta', 0100, 0101, 0100);
insert into match (id, date, location, team1_id, team2_id, winner_id)
values (01001, '2020-05-12', 'Atlanta', 0100, 0102, 0102);
insert into match (id, date, location, team1_id, team2_id, winner_id)
values (01002, '2020-05-12', 'Atlanta', 0100, 0103, 0103);
insert into match (id, date, location, team1_id, team2_id, winner_id)
values (01003, '2020-05-12', 'Atlanta', 0101, 0102, 0101);
insert into match (id, date, location, team1_id, team2_id, winner_id)
values (01004, '2020-05-12', 'Atlanta', 0101, 0103, 0103);
insert into match (id, date, location, team1_id, team2_id, winner_id)
values (01005, '2020-05-12', 'Atlanta', 0102, 0103, 0103);
insert into player(id, name, elo, team_id)
values (010000, 'Eric Ritchie', 2631, 0100);
insert into player(id, name, elo, team_id)
values (010001, 'Robert Huang', 2311, 0100);
insert into player(id, name, elo, team_id)
values (010002, 'Yasin Dinçer', 2422, 0100);
insert into player(id, name, elo, team_id)
values (010003, 'Jesper Svenningsen', 2534, 0100);
insert into player(id, name, elo, team_id)
values (010004, 'Philippe Laflamme', 2467, 0100);
insert into player(id, name, elo, team_id)
values (010005, 'Heo Seung-hoon', 2654, 0101);
insert into player(id, name, elo, team_id)
values (010006, 'Dennis Johnsen', 2611, 0101);
insert into player(id, name, elo, team_id)
values (010007, 'Daniele di Mauro', 22231, 0101);
insert into player(id, name, elo, team_id)
values (010008, 'Bae Jun-sik', 2298, 0101);
insert into player(id, name, elo, team_id)
values (010009, 'Tristan Stidam', 2389, 0101);
insert into player(id, name, elo, team_id)
values (010010, 'Soren Bjerg', 2665, 0104);
insert into player(id, name, elo, team_id)
values (010011, 'Erik Wessen', 1998, 0104);
insert into player(id, name, elo, team_id)
values (010012, 'Ming Lu', 2313, 0104);
insert into player(id, name, elo, team_id)
values (010013, 'Vincent Wang', 2005, 0104);
insert into player(id, name, elo, team_id)
values (010014, 'Jackson Dohan', 2411, 0104);
insert into "user" (id, email, first_name, last_name, username)
values (1, 'test@test.com', 'test', 'test', 'test2');
insert into "user" (id, email, first_name, last_name, username)
values (2, 'test@test.com', 'test', 'test', 'test');
insert into user_leagues (user_id, leagues_id)
values (1, 10);
insert into user_leagues (user_id, leagues_id)
values (1, 11);
insert into user_leagues (user_id, leagues_id)
values (2, 10);
insert into tag (id, type_Id, type, name)
values (100000, 01, 'sport','League of Legends');
insert into tag (id, type_Id, type, name)
values (100001, 02, 'sport','Dota2');
insert into tag (id, type_Id, type, name)
values (100002, 100, 'team','Cloud9');
insert into tag (id, type_Id, type, name)
values (100003, 101, 'team','Evil Geniuses');
insert into blog (id, content, cover, date, published, title, user_id)
values (1000000, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy ',
        'https://gitlab.com/sasep-clairvoyance/sasep-frontend/raw/develop/src/assets/images/logos/blog/mementomori.png', '2020-05-12', true, 'Memento Mori', 2);
insert into blog (id, content, cover, date, published, title, user_id)
values (1000001,
        'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros. ',
        'https://gitlab.com/sasep-clairvoyance/sasep-frontend/raw/develop/src/assets/images/logos/blog/ipsum.png', '2020-05-12', true, 'Lorem Ipsum', 2);
insert into blog (id, content, cover, date, published, title, user_id)
values (1000002,
        'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim',
        'https://gitlab.com/sasep-clairvoyance/sasep-frontend/raw/develop/src/assets/images/logos/blog/ipsum.png', '2020-05-12', true, 'Carpe Diem', 1);
insert into blog (id, content, cover, date, published, title, user_id)
values (1000004,
        'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum',
        'https://gitlab.com/sasep-clairvoyance/sasep-frontend/raw/develop/src/assets/images/logos/blog/mementomori.png', '2020-05-15', true, 'Ecetera', 2);
insert into blog_tags(blog_id, tags_id)
values (1000000, 100000);
insert into blog_tags(blog_id, tags_id)
values (1000000, 100001);
insert into blog_tags(blog_id, tags_id)
values (1000000, 100002);
insert into blog_tags(blog_id, tags_id)
values (1000001, 100003);