package io.clairvoyance.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Validated
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @JsonIgnore
    private Long id;

    @JsonProperty("id")
    private Long typeId;

    @JsonProperty("type") //Sport or Team
    private String type;

    @JsonProperty("name") //sportName or teamName
    private String name;

    public Tag id(Long id) {
        this.id = id;
        return this;
    }

    @NotNull
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Tag typeId(Long typeId) {
        this.typeId = typeId;
        return this;
    }

    @ApiModelProperty
    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Tag type(String type) throws UnknownTypeException {
        setType(type);
        return this;
    }

    @ApiModelProperty
    @NotNull
    public String getType() {
        return type;
    }

    public void setType(String type) throws UnknownTypeException {
        if (!(type.equals("team") || type.equals("sport"))) {
            throw new UnknownTypeException("This type is unknown: " + type);
        }
        this.type = type;
    }

    public Tag name(String name) {
        this.name = name;
        return this;
    }

    @ApiModelProperty
    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tag)) return false;
        Tag tag = (Tag) o;
        return id.equals(tag.id) &&
                Objects.equals(typeId, tag.typeId) &&
                type.equals(tag.type) &&
                name.equals(tag.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, typeId, type, name);
    }

    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", typeId=" + typeId +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
