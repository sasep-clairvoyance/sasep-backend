package io.clairvoyance.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Entity
@Validated
public class League {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("logo")
    private String logo = null;

    @JsonProperty("private")
    private Boolean privacy = false;

    @JsonProperty("owner")
    private String owner = null;

    @ManyToOne
    @JoinColumn(name = "sport_id")
    @JsonIgnore
    private Sport sport = null;

    @ManyToMany
    @JsonIgnore
    private List<User> users = null;

    @JsonProperty("sportId")
    private Long getSportId() {
        return sport.getId();
    }

    public League() {
    }

    public League(Long id, String name, String logo, Boolean privacy, String owner, List<User> users) {
        this.id = id;
        this.name = name;
        this.logo = logo;
        this.privacy = privacy;
        this.owner = owner;
        this.users = users;
    }

    public League id(Long id) {
        this.id = id;
        return this;
    }

    @ApiModelProperty
    @NotNull
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public League name(String name) {
        this.name = name;
        return this;
    }

    @ApiModelProperty
    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public League logo(String logo) {
        this.logo = logo;
        return this;
    }

    @ApiModelProperty
    @NotNull
    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public League privacy(boolean privacy) {
        this.privacy = privacy;
        return this;
    }

    @ApiModelProperty
    @NotNull
    public Boolean getPrivacy() {
        return privacy;
    }

    public void setPrivacy(Boolean privacy) {
        this.privacy = privacy;
    }

    public League owner(String owner) {
        this.owner = owner;
        return this;
    }

    @ApiModelProperty
    public Optional<String> getOwner() {
        return Optional.ofNullable(owner);
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public League sport(Sport sport) {
        this.sport = sport;
        return this;
    }

    public Sport getSport() {
        return sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }

    public League users(User user) {
        this.users.add(user);
        return this;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> user) {
        this.users = user;
    }

    public void addUsers(User user) {
        if(this.users == null) {
            this.users = new ArrayList<>();
        }
        this.users.add(user);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof League)) return false;
        League league = (League) o;
        return id.equals(league.id) &&
                name.equals(league.name) &&
                logo.equals(league.logo) &&
                privacy.equals(league.privacy) &&
                Objects.equals(owner, league.owner) &&
                sport.equals(league.sport) &&
                Objects.equals(users, league.users);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, logo, privacy, owner, sport, users);
    }

    @Override
    public String toString() {
        return "League object: {\n" +
                id + "\n" +
                name + "\n" +
                logo + "\n" +
                privacy + "\n" +
                owner + "\n" +
                users + "\n" +
                "}";
    }
}