package io.clairvoyance.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;

@Entity
@Validated
@Table(name = "\"User\"")
@ApiModel(description = "Class representing a user tracked by the application.")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @JsonProperty("id")
    private Long id = null;

    @ApiModelProperty(notes = "The username is extracted by the authentication\n" +
                              "Unique identifier of the user to log in. No two users can have the same username. " +
                              "The username can't be changed once an account has been created. " +
                              "The username is extracted by the authentication",
                      example = "maxmustermann",
                      position = 0)
    @JsonProperty("username")
    private String username = null;

    @ApiModelProperty(notes = "mail address of the user.",
                      example = "max@test.de",
                      position = 1)
    @JsonProperty("email")
    private String email = null;

    @ApiModelProperty(notes = "first name of the user",
                      example = "max",
                      position = 2)
    @JsonProperty("first_name")
    private String first_name;

    @ApiModelProperty(notes = "last name of the user.",
                      example = "mustermann",
                      position = 3)
    @JsonProperty("last_name")
    private String last_name;

    @JsonProperty("interestedInTeam")
    @Valid
    @ManyToMany
    private Set<Team> interestedInTeam;

    @JsonProperty("interestedInSport")
    @Valid
    @ManyToMany
    private Set<Sport> interestedInSport;

    @ManyToMany
    @JsonIgnore
    private List<League> leagues = null;

    public User() {
    }

    public User(String username, String email, String first_name, String last_name) {
        this.username = username;
        this.email = email;
        this.first_name = first_name;
        this.last_name = last_name;
    }

    public User id(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     **/
    @ApiModelProperty(required = true)
    @NotNull

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User username(String username) {
        this.username = username;
        return this;
    }

    /**
     * Get username
     *
     * @return username
     **/
    @ApiModelProperty(example = "TheLegend27", required = true)
    @NotNull

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public User email(String email) {
        this.email = email;
        return this;
    }

    /**
     * Get email
     *
     * @return email
     **/
    @ApiModelProperty(example = "no-reply@mail.gg", required = true)
    @NotNull

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public User interestedInTeam(Set<Team> interestedInTeam) {
        this.interestedInTeam = interestedInTeam;
        return this;
    }

    public boolean addInterestedInTeamItem(Team interestedInTeamItem) {
        if (this.interestedInTeam == null) {
            this.interestedInTeam = new HashSet<>();
        }
        return this.interestedInTeam.add(interestedInTeamItem);
    }

    public boolean removeInterestedInTeamItem(Team interestedInTeamItem) {
        return this.interestedInTeam.removeIf(team -> team.equals(interestedInTeamItem));
    }

    /**
     * Get interestedInTeam
     *
     * @return interestedInTeam
     **/
    @ApiModelProperty()
    @Valid
    public Set<Team> getInterestedInTeam() {
        return interestedInTeam;
    }

    public void setInterestedInTeam(Set<Team> interestedInTeam) {
        this.interestedInTeam = interestedInTeam;
    }

    public User interestedInSport(Set<Sport> interestedInSport) {
        this.interestedInSport = interestedInSport;
        return this;
    }

    public boolean addInterestedInSportItem(Sport interestedInSportItem) {
        if (this.interestedInSport == null) {
            this.interestedInSport = new HashSet<>();
        }
        return this.interestedInSport.add(interestedInSportItem);
    }

    public boolean removeInterestedInSportItem(Sport interestedInSportItem) {
        return this.interestedInSport.removeIf(sport -> sport.equals(interestedInSportItem));
    }

    /**
     * Get interestedInSport
     *
     * @return interestedInSport
     **/
    @ApiModelProperty()
    @Valid
    public Set<Sport> getInterestedInSport() {
        return interestedInSport;
    }

    public void setInterestedInSport(Set<Sport> interestedInSport) {
        this.interestedInSport = interestedInSport;
    }

    public List<League> getLeagues() {
        return leagues;
    }

    public void setLeagues(List<League> leagues) {
        this.leagues = leagues;
    }

    public void addLeagues(League league) {
        if(this.leagues == null) {
            this.leagues = new ArrayList<>();
        }

        if (league.getPrivacy()) {
            this.leagues.add(league);
        }
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return Objects.equals(this.id, user.id) &&
               Objects.equals(this.username, user.username) &&
               Objects.equals(this.email, user.email) &&
               Objects.equals(this.first_name, user.first_name) &&
               Objects.equals(this.last_name, user.last_name) &&
               Objects.equals(this.interestedInTeam, user.interestedInTeam) &&
               Objects.equals(this.interestedInSport, user.interestedInSport) &&
                Objects.equals(this.leagues, user.leagues);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, email, first_name, last_name, interestedInTeam, interestedInSport, leagues);
    }

    @Override
    public String toString() {
        return "class User {\n" +
                "    id: " + toIndentedString(id) + "\n" +
                "    username: " + toIndentedString(username) + "\n" +
                "    email: " + toIndentedString(email) + "\n" +
                "    first_name: " + toIndentedString(first_name) + "\n" +
                "    last_name: " + toIndentedString(last_name) + "\n" +
                "    interestedInTeam: " + toIndentedString(interestedInTeam) + "\n" +
                "    interestedInSport: " + toIndentedString(interestedInSport) + "\n" +
                "    leagues: " + toIndentedString(leagues) + "\n" +
                "}";
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

