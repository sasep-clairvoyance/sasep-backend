package io.clairvoyance.model;

import java.util.Objects;

public class PrivateLeagueRightRequest extends RightRequest {

    private Long leagueId;
    private String message;

    public PrivateLeagueRightRequest(String type, Long leagueId, String message) {
        this.leagueId = leagueId;
        this.message = message;
    }

    @Override
    public void sendEmail(User user) {
        System.out.println("##########");
        System.out.println(user.getUsername() + " requested a right:");
        System.out.println(this);
        System.out.println("##########");
    }

    public Long getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(Long leagueId) {
        this.leagueId = leagueId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PrivateLeagueRightRequest)) return false;
        PrivateLeagueRightRequest that = (PrivateLeagueRightRequest) o;
        return Objects.equals(leagueId, that.leagueId) &&
               Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(leagueId, message);
    }

    @Override
    public String toString() {
        return "PrivateLeagueRightRequest{" +
               "leagueId=" + leagueId +
               ", message='" + message + '\'' +
               '}';
    }
}
