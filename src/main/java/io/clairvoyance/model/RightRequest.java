package io.clairvoyance.model;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
                      @JsonSubTypes.Type(value = ContributionRightRequest.class, name = "contribution-rights"),
                      @JsonSubTypes.Type(value = PrivateLeagueRightRequest.class, name = "league-access"),
              })
public abstract class RightRequest {

    public abstract void sendEmail(User user);
}
