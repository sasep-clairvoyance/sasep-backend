package io.clairvoyance.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Validated
public class Blog {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @JsonProperty("id")
    private Long id;

    @JsonProperty
    private String title;

    @JsonProperty
    @Column(length = 10000000)
    private String cover;

    @JsonProperty
    private String content;

    @JsonProperty
    private Date date;

    @OneToMany(cascade = CascadeType.ALL)
    @JsonProperty
    private List<Tag> tags;

    @JsonProperty
    private Boolean published = false;

    @JsonIgnore
    @ManyToOne
    private User user;

    @JsonProperty("author")
    public String getAuthor() {
        return user.getUsername();
    }

    public Blog(String title, String cover, String content, Date date, List<Tag> tags, Boolean published) {
        this.title = title;
        this.cover = cover;
        this.content = content;
        this.date = date;
        this.tags = tags;
        this.published = published;
    }

    public Blog() {
    }

    public Blog id(Long id) {
        this.id = id;
        return this;
    }

    @ApiModelProperty
//    @NotNull
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Blog title(String title) {
        this.title = title;
        return this;
    }

    @ApiModelProperty
    @NotNull
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public Blog cover(String cover) {
        this.cover = cover;
        return this;
    }

    @ApiModelProperty
    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public Blog content(String content) {
        this.content = content;
        return this;
    }

    @ApiModelProperty
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public Blog publishingDate(Date publishingDate) {
        this.date = publishingDate;
        return this;
    }

    @ApiModelProperty
    public Date getDate() {
        return date;
    }

    public void setDate(Date publishingDate) {
        this.date = publishingDate;
    }


    public Blog tags(List<Tag> tags) {
        this.tags = tags;
        return this;
    }

    @ApiModelProperty
    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public void addTags(Tag tag) {
        if (this.tags == null) {
            this.tags = new ArrayList<>();
        }
        this.tags.add(tag);
    }


    public Blog published(Boolean published) {
        this.published = published;
        return this;
    }

    @ApiModelProperty
    public Boolean getPublished() {
        return published;
    }

    public void setPublished(Boolean published) {
        this.published = published;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Blog)) return false;
        Blog blog = (Blog) o;
        return id.equals(blog.id) &&
                Objects.equals(title, blog.title) &&
                Objects.equals(cover, blog.cover) &&
                Objects.equals(content, blog.content) &&
                Objects.equals(date, blog.date) &&
                Objects.equals(tags, blog.tags) &&
                Objects.equals(published, blog.published) &&
                Objects.equals(user, blog.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, cover, content, date, tags, published, user);
    }

    @Override
    public String toString() {
        return "{" +
                "id: " + id +
                ", title: " + title + '\'' +
                ", cover: " + cover + '\'' +
                ", content: '" + content + '\'' +
                ", publishingDate: " + date +
                ", tags: " + tags +
                ", published: " + published +
                ", user: " + user +
                '}';
    }
}
