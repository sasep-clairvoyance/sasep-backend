package io.clairvoyance.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * CompareBody
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T09:57:49.374Z[GMT]")
public class CompareBody {
    @JsonProperty("team-1-id")
    private Long team1Id = null;

    @JsonProperty("team-2-id")
    private Long team2Id = null;

    public CompareBody team1Id(Long team1Id) {
        this.team1Id = team1Id;
        return this;
    }

    /**
     * Get team1Id
     *
     * @return team1Id
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public Long getTeam1Id() {
        return team1Id;
    }

    public void setTeam1Id(Long team1Id) {
        this.team1Id = team1Id;
    }

    public CompareBody team2Id(Long team2Id) {
        this.team2Id = team2Id;
        return this;
    }

    /**
     * Get team2Id
     *
     * @return team2Id
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public Long getTeam2Id() {
        return team2Id;
    }

    public void setTeam2Id(Long team2Id) {
        this.team2Id = team2Id;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CompareBody compareBody = (CompareBody) o;
        return Objects.equals(this.team1Id, compareBody.team1Id) &&
                Objects.equals(this.team2Id, compareBody.team2Id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(team1Id, team2Id);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class CompareBody {\n");

        sb.append("    team1Id: ").append(toIndentedString(team1Id)).append("\n");
        sb.append("    team2Id: ").append(toIndentedString(team2Id)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
