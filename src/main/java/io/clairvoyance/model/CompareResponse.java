package io.clairvoyance.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;

/**
 * CompareResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T09:57:49.374Z[GMT]")
public class CompareResponse {
    @JsonProperty("winner")
    private Long winner = null;

    @JsonProperty("result")
    private Double result = null;

    public CompareResponse winner(Long winner) {
        this.winner = winner;
        return this;
    }

    /**
     * The winners Id
     *
     * @return winner
     **/
    @ApiModelProperty(value = "The winners Id")

    public Long getWinner() {
        return winner;
    }

    public void setWinner(Long winner) {
        this.winner = winner;
    }

    public CompareResponse result(Double result) {
        this.result = result;
        return this;
    }

    /**
     * Get result
     *
     * @return result
     **/
    @ApiModelProperty(value = "")

    public Double getResult() {
        return result;
    }

    public void setResult(Double result) {
        this.result = result;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CompareResponse compareResponse = (CompareResponse) o;
        return Objects.equals(this.winner, compareResponse.winner) &&
                Objects.equals(this.result, compareResponse.result);
    }

    @Override
    public int hashCode() {
        return Objects.hash(winner, result);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");
        sb.append("    result: ").append(toIndentedString(result)).append("\n");
        sb.append("    winner: ").append(toIndentedString(winner)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

    public void compare(Team team1, Team team2) {
        findWinner(team1, team2);
        findResult(team1, team2);
    }

    private void findResult(Team team1, Team team2) {
        Integer winnerElo;
        Integer loserElo;
        if (team1.getId().equals(winner)) {
            winnerElo = team1.getElo();
            loserElo = team2.getElo();
        } else {
            winnerElo = team2.getElo();
            loserElo = team1.getElo();
        }
        this.result = ((double) (winnerElo)) / (winnerElo + loserElo);
    }

    private void findWinner(Team team1, Team team2) {
        Long winner = team1.getElo() > team2.getElo() ? team1.getId() : team2.getId(); //draws not accountend for
        this.winner = winner;
    }
}
