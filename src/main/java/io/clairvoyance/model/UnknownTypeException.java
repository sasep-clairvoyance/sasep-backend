package io.clairvoyance.model;

public class UnknownTypeException extends Exception {
    public UnknownTypeException(String message) {
        super(message);
    }
}
