package io.clairvoyance.model;

import java.util.List;
import java.util.Objects;

public class ContributionRightRequest extends RightRequest {

    private String contributionTo;
    private List<String> contributionTypes;
    private String application;

    public ContributionRightRequest(String type, String contributionTo, List<String> contributionTypes, String application) {
        this.contributionTo = contributionTo;
        this.contributionTypes = contributionTypes;
        this.application = application;
    }

    @Override
    public void sendEmail(User user) {
        System.out.println("##########");
        System.out.println(user.getUsername() + " requested a right:");
        System.out.println(this);
        System.out.println("##########");
    }

    public String getContributionTo() {
        return contributionTo;
    }

    public void setContributionTo(String contributionTo) {
        this.contributionTo = contributionTo;
    }

    public List<String> getContributionTypes() {
        return contributionTypes;
    }

    public void setContributionTypes(List<String> contributionTypes) {
        this.contributionTypes = contributionTypes;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ContributionRightRequest)) return false;
        ContributionRightRequest that = (ContributionRightRequest) o;
        return Objects.equals(contributionTo, that.contributionTo) &&
               Objects.equals(contributionTypes, that.contributionTypes) &&
               Objects.equals(application, that.application);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contributionTo, contributionTypes, application);
    }

    @Override
    public String toString() {
        return "ContributionRightRequest{" +
               "contributionTo='" + contributionTo + '\'' +
               ", contributionTypes=" + contributionTypes +
               ", application='" + application + '\'' +
               '}';
    }
}
