package io.clairvoyance.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

/**
 * Match
 */
@Entity
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T09:57:49.374Z[GMT]")
public class Match {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("date")
    private Date date = null;

    @JsonProperty("location")
    private String location = null;

    @ManyToOne
    @JsonProperty("team1")
    @JoinColumn(name = "team1_id")
    private Team team1 = null;

    @ManyToOne
    @JsonProperty("team2")
    @JoinColumn(name = "team2_id")
    private Team team2 = null;

    @ManyToOne
    @JsonProperty("winner")
    @JoinColumn(name = "winner_id")
    private Team winner = null;


    public Match id(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Match date(Date date) {
        this.date = date;
        return this;
    }

    /**
     * Get date
     *
     * @return date
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Match location(String location) {
        this.location = location;
        return this;
    }

    /**
     * Get location
     *
     * @return location
     **/
    @ApiModelProperty(example = "Karlsruhe", required = true, value = "")
    @NotNull

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Match teams(Team team1) {
        this.team1 = team1;
        return this;
    }

    /**
     * Get teams
     *
     * @return teams
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull
    public Team getTeam1() {
        return team1;
    }

    public void setTeam1(Team team1) {
        this.team1 = team1;
    }

    public Match team2(Team team2) {
        this.team2 = team2;
        return this;
    }

    /**
     * Get team2
     *
     * @return team2
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull
    public Team getTeam2() {
        return team2;
    }

    public void setTeam2(Team team2) {
        this.team2 = team2;
    }

    public Match winner(Team winner) {
        setWinner(winner);
        return this;
    }

    public Team getWinner() {
        return winner;
    }

    public void setWinner(Team winner) {
        if (winner.equals(team1)) {
            this.winner = winner;
        }
        if (winner.equals(team2)) {
            this.winner = winner;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Match)) return false;
        Match match = (Match) o;
        return id.equals(match.id) &&
                Objects.equals(date, match.date) &&
                Objects.equals(location, match.location) &&
                team1.equals(match.team1) &&
                team2.equals(match.team2) &&
                Objects.equals(winner, match.winner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, location, team1, team2, winner);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    date: ").append(toIndentedString(date)).append("\n");
        sb.append("    location: ").append(toIndentedString(location)).append("\n");
        sb.append("    team one: ").append(toIndentedString(team1)).append("\n");
        sb.append("    team two: ").append(toIndentedString(team2)).append("\n");
        if (winner != null) {
            sb.append("    winner: ").append(toIndentedString(winner)).append("\n");
        }
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
