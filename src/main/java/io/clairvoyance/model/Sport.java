package io.clairvoyance.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

/**
 * Sport
 */
@Entity
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T09:57:49.374Z[GMT]")
public class Sport {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("thumbnailUrl")
    private String thumbnailUrl = null;

    @JsonProperty("backColor")
    private String backColor = null;

    @JsonProperty("logo")
    private String imageUrl = null;

    @ElementCollection
    @JsonProperty("leagues")
    @JoinColumn(name = "sport_id")
    private List<League> leagues = null;

    public Sport id(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Sport name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     **/
    @ApiModelProperty(example = "League of Legends", required = true, value = "")
    @NotNull

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sport imageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    /**
     * Get imageUrl
     *
     * @return imageUrl
     **/
    @ApiModelProperty(example = "/logos/sports/logo.png", value = "")

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Sport backColor(String backColor) {
        this.backColor = backColor;
        return this;
    }

    /**
     * Get backColor
     *
     * @return backColor
     **/
    @ApiModelProperty(example = "green", value = "")

    public String getBackColor() {
        return backColor;
    }

    public void setBackColor(String backColor) {
        this.backColor = backColor;
    }


    public Sport thumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
        return this;
    }

    /**
     * Get thumbnailUrl
     *
     * @return thumbnailUrl
     **/
    @ApiModelProperty(example = "/logos/sports/thumb.png")
    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public Sport leagues(List<League> leagues) {
        this.leagues = leagues;
        return this;
    }

    @ApiModelProperty
    public List<League> getLeagues() {
        return leagues;
    }

    public void setLeagues(List<League> leagues) {
        this.leagues = leagues;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Sport sport = (Sport) o;
        return Objects.equals(this.id, sport.id) &&
                Objects.equals(this.name, sport.name) &&
                Objects.equals(this.imageUrl, sport.imageUrl) &&
                Objects.equals(this.backColor, sport.backColor) &&
                Objects.equals(this.thumbnailUrl, sport.thumbnailUrl) &&
                Objects.equals(this.leagues, sport.leagues);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, imageUrl, backColor, thumbnailUrl, leagues);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    imageUrl: ").append(toIndentedString(imageUrl)).append("\n");
        sb.append("    backColor: ").append(toIndentedString(backColor)).append("\n");
        sb.append("    thumbnailUrl: ").append(toIndentedString(thumbnailUrl)).append("\n");
        sb.append("    leagues: ").append(toIndentedString(leagues)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
