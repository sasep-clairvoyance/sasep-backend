package io.clairvoyance.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * SearchResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T09:57:49.374Z[GMT]")
public class SearchResponse {
  @JsonProperty("sports")
  @Valid
  private List<Sport> sports = null;

  @JsonProperty("teams")
  @Valid
  private List<Team> teams = null;

  @JsonProperty("players")
  @Valid
  private List<Player> players = null;

  public SearchResponse sports(List<Sport> sports) {
    this.sports = sports;
    return this;
  }

  public SearchResponse addSportsItem(Sport sportsItem) {
    if (this.sports == null) {
      this.sports = new ArrayList<Sport>();
    }
    this.sports.add(sportsItem);
    return this;
  }

  /**
   * Get sports
   * @return sports
  **/
  @ApiModelProperty(value = "")
      @Valid
    public List<Sport> getSports() {
    return sports;
  }

  public void setSports(List<Sport> sports) {
    this.sports = sports;
  }

  public SearchResponse teams(List<Team> teams) {
    this.teams = teams;
    return this;
  }

  public SearchResponse addTeamsItem(Team teamsItem) {
    if (this.teams == null) {
      this.teams = new ArrayList<Team>();
    }
    this.teams.add(teamsItem);
    return this;
  }

  /**
   * Get teams
   * @return teams
  **/
  @ApiModelProperty(value = "")
      @Valid
    public List<Team> getTeams() {
    return teams;
  }

  public void setTeams(List<Team> teams) {
    this.teams = teams;
  }

  public SearchResponse players(List<Player> players) {
    this.players = players;
    return this;
  }

  public SearchResponse addPlayersItem(Player playersItem) {
    if (this.players == null) {
      this.players = new ArrayList<Player>();
    }
    this.players.add(playersItem);
    return this;
  }

  /**
   * Get players
   * @return players
  **/
  @ApiModelProperty(value = "")
      @Valid
    public List<Player> getPlayers() {
    return players;
  }

  public void setPlayers(List<Player> players) {
    this.players = players;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SearchResponse searchResponse = (SearchResponse) o;
    return Objects.equals(this.sports, searchResponse.sports) &&
        Objects.equals(this.teams, searchResponse.teams) &&
        Objects.equals(this.players, searchResponse.players);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sports, teams, players);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SearchResponse {\n");
    
    sb.append("    sports: ").append(toIndentedString(sports)).append("\n");
    sb.append("    teams: ").append(toIndentedString(teams)).append("\n");
    sb.append("    players: ").append(toIndentedString(players)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
