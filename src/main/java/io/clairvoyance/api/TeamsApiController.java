package io.clairvoyance.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.clairvoyance.model.Player;
import io.clairvoyance.model.Team;
import io.clairvoyance.repository.EntryNotFoundException;
import io.clairvoyance.service.TeamService;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T09:57:49.374Z[GMT]")
@Controller
public class TeamsApiController implements TeamsApi {

    private static final Logger log = LoggerFactory.getLogger(TeamsApiController.class);
    private final ObjectMapper objectMapper;
    private final HttpServletRequest request;
    @Autowired
    private TeamService service;

    @Autowired
    public TeamsApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Override
    public ResponseEntity<List<Team>> teams() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            return ResponseEntity.ok(service.getAllTeams());
        }

        return new ResponseEntity<List<Team>>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ResponseEntity<Team> team(@ApiParam(value = "The Team ID", required = true) @PathVariable("teamId") Long teamId) {
        String accept = request.getHeader("Accept");
        System.out.println(accept);
        if (accept != null && accept.contains("application/json")) {
            try {
                return ResponseEntity.ok(service.getTeamById(teamId));
            } catch (EntryNotFoundException e) {
                log.error("Could not get team", e);
                return new ResponseEntity<Team>(HttpStatus.NOT_FOUND);
            }
        }

        return new ResponseEntity<Team>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ResponseEntity<List<Player>> teamPlayers(@ApiParam(value = "The Team Id", required = true) @PathVariable("teamId") Long teamId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                service.getTeamById(teamId);
                return ResponseEntity.ok(service.getPlayersByTeamId(teamId));
            } catch (EntryNotFoundException e) {
                log.error("Could not get team", e);
                return new ResponseEntity<List<Player>>(HttpStatus.NOT_FOUND);
            }
        }

        return new ResponseEntity<List<Player>>(HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
