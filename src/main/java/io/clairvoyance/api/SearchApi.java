package io.clairvoyance.api;

import io.clairvoyance.model.SearchBody;
import io.clairvoyance.model.SearchResponse;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Api(value = "search", description = "the search API")
public interface SearchApi {

    @ApiOperation(value = "search for a Sport/Team/Player", nickname = "serach", notes = "", response = SearchResponse.class, tags = {"guests", "users",})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Results found", response = SearchResponse.class),
            @ApiResponse(code = 404, message = "No results found")})
    @RequestMapping(value = "/search",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<SearchResponse> search(@ApiParam(value = "The search request") @Valid @RequestBody SearchBody body);

}
