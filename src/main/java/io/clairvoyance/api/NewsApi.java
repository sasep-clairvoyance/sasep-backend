package io.clairvoyance.api;

import io.clairvoyance.model.Blog;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(value = "news")
public interface NewsApi {
    @ApiOperation(value = "Returns all blog articles", nickname = "blog", response = Blog.class, responseContainer = "List", tags = {"users", "guests",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Blog found", response = Blog.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Blog not found")})
    @RequestMapping(value = "/news",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<Blog>> returnAllBlogArticles(@RequestParam(value = "simple", defaultValue = "false", required = false) Boolean simple,
                                                     @RequestParam(value = "sort", defaultValue = "latest", required = false) String sort,
                                                     @RequestParam(value = "sport", required = false) Long sportId,
                                                     @RequestParam(value = "team", required = false) Long teamId,
                                                     @RequestParam(value = "no-cover", defaultValue = "false", required = false) Boolean noCover,
                                                     @RequestParam(value = "only-published", defaultValue = "true", required = false) Boolean onlyPublished);

    @ApiOperation(value = "Returns one article", nickname = "blogById", response = Blog.class, tags = {"users", "guests",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Blog article found", response = Blog.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Blog article not found")})
    @RequestMapping(value = "/news/{blogId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<Blog> returnBlogArticleById(@ApiParam(value = "The blog article id", required = true) @PathVariable("blogId") Long blogId);

    @ApiOperation(value = "Adds a Blog ", nickname = "news", authorizations = {
            @Authorization(value = "bearerAuth")}, tags = {"users",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Blog saved"),
            @ApiResponse(code = 400, message = "Bad request")})
    @RequestMapping(value = "/news",
            consumes = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<Long> blog(@ApiParam @Valid @RequestBody Blog blog);

    @ApiOperation(value = "Update a Blog ", nickname = "news", authorizations = {
            @Authorization(value = "bearerAuth")}, tags = {"users",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Blog updated"),
            @ApiResponse(code = 400, message = "Bad request")})
    @RequestMapping(value = "/news/{blogId}",
            consumes = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<Void> blogupdate(@ApiParam(value = "The Blog Id", required = true) @PathVariable("blogId") Long blogId, @ApiParam @Valid @RequestBody Blog blog);

}
