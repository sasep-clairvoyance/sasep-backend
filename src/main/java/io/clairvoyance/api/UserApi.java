package io.clairvoyance.api;

import io.clairvoyance.model.*;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "user", description = "the user API")
public interface UserApi {

    @ApiOperation(value = "Removes a Sport from the User's interest List", nickname = "removeSportInterest", notes = "", authorizations = {
            @Authorization(value = "bearerAuth")}, tags = {"users",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Interest deleted"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Sport not found")})
    @RequestMapping(value = "/user/sports/{sportId}",
                    method = RequestMethod.DELETE)
    ResponseEntity<Void> removeSportInterest(@ApiParam(value = "The Sport Id", required = true) @PathVariable("sportId") Long sportId);


    @ApiOperation(value = "Delete a Team from the User`s interest List", nickname = "removeTeamInterest", notes = "", authorizations = {
            @Authorization(value = "bearerAuth")}, tags = {"users",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Interest removed"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Interest not found")})
    @RequestMapping(value = "/user/teams/{teamId}",
                    method = RequestMethod.DELETE)
    ResponseEntity<Void> removeTeamInterest(@ApiParam(value = "The Team Id", required = true) @PathVariable("teamId") Long teamId);


    @ApiOperation(value = "Adds a Sport to the User's interest List", nickname = "sportInterest", notes = "", authorizations = {
            @Authorization(value = "bearerAuth")}, tags = {"users",})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Sport added to List"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 401, message = "Unauthorized")})
    @RequestMapping(value = "/user/sports/{sportId}",
                    method = RequestMethod.PUT)
    ResponseEntity<Void> sportInterest(@ApiParam(value = "The Sport Id", required = true) @PathVariable("sportId") Long sportId);


    @ApiOperation(value = "Adds a Team to the User's interest List", nickname = "teamInterest", notes = "", authorizations = {
            @Authorization(value = "bearerAuth")}, tags = {"users",})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Team added to List"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Ressource not found")})
    @RequestMapping(value = "/user/teams/{teamId}",
                    method = RequestMethod.PUT)
    ResponseEntity<Void> teamInterest(@ApiParam(value = "The Team Id", required = true) @PathVariable("teamId") Long teamId);


    @ApiOperation(value = "Gets User info", nickname = "user", notes = "", response = User.class, authorizations = {
            @Authorization(value = "bearerAuth")}, tags = {"users",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User found", response = User.class),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "User not found")})
    @RequestMapping(value = "/user",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<User> user();


    @ApiOperation(value = "Returns User's list of interested Sports", nickname = "userSports", notes = "", response = Sport.class, responseContainer = "List", authorizations = {
            @Authorization(value = "bearerAuth")}, tags = {"users",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User's TeamList found", response = Sport.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Empty List")})
    @RequestMapping(value = "/user/sports",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<List<Sport>> userSports();


    @ApiOperation(value = "Returns User's list of interested Teams", nickname = "userTeams", notes = "", response = Team.class, responseContainer = "List", authorizations = {
            @Authorization(value = "bearerAuth")}, tags = {"users",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User's TeamList found", response = Team.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Empty List")})
    @RequestMapping(value = "/user/teams",
                    produces = {"application/json"},
                    method = RequestMethod.GET)
    ResponseEntity<List<Team>> userTeams();

    @ApiOperation(value = "Returns User's list of blogposts", nickname = "usersBlogs", response = Blog.class, responseContainer = "List", authorizations = {
            @Authorization(value = "bearerAuth")}, tags = {"users",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User's Blogposts found", response = Blog.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Empty List")})
    @RequestMapping(value = "/user/contribution/news",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<Blog>> userBlogs(@RequestParam(value = "simple", defaultValue = "false", required = false) Boolean simple,
                                         @RequestParam(value = "sort", defaultValue = "latest", required = false) String sort,
                                         @RequestParam(value = "sport", required = false) Long sportId,
                                         @RequestParam(value = "team", required = false) Long teamId,
                                         @RequestParam(value = "no-cover", defaultValue = "false", required = false) Boolean noCover,
                                         @RequestParam(value = "limit", defaultValue = "10", required = false) Long limit);

    @ApiOperation(value = "Returns User's list of rights to private leagues", nickname = "userRights", notes = "", response = League.class, responseContainer = "List", authorizations = {
            @Authorization(value = "bearerAuth")}, tags = {"users",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User's LeagueList found", response = League.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "Empty List")})
    @RequestMapping(value = "/user/rights/leagues",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<Long>> userRights();

    @ApiOperation(value = "Requests right for the current user.", nickname = "right requests", notes = "", response = Team.class, authorizations = {
            @Authorization(value = "bearerAuth")}, tags={ "users", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Request successful"),
            @ApiResponse(code = 400, message = "Bad Request")})
    @RequestMapping(value = "/user/requests",
                    produces = { "application/json" },
                    method = RequestMethod.POST)
    ResponseEntity<Void> requestRight(@RequestBody RightRequest request);

}
