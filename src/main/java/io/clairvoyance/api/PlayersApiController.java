package io.clairvoyance.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.clairvoyance.model.Player;
import io.clairvoyance.repository.EntryNotFoundException;
import io.clairvoyance.service.PlayerService;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T09:57:49.374Z[GMT]")
@Controller
public class PlayersApiController implements PlayersApi {

    private static final Logger log = LoggerFactory.getLogger(PlayersApiController.class);
    private final ObjectMapper objectMapper;
    private final HttpServletRequest request;
    @Autowired
    private PlayerService playerService;

    @Autowired
    public PlayersApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Player> player(@ApiParam(value = "The Player ID", required = true) @PathVariable("playerId") Long playerId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return ResponseEntity.ok(playerService.getPlayerById(playerId));
            } catch (EntryNotFoundException e) {
                log.error("Could not get player", e);
                return new ResponseEntity<Player>(HttpStatus.NOT_FOUND);
            }
        }

        return new ResponseEntity<Player>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
