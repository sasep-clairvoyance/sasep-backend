package io.clairvoyance.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.clairvoyance.model.*;
import io.clairvoyance.repository.EntryNotFoundException;
import io.clairvoyance.service.BlogService;
import io.clairvoyance.service.SportService;
import io.clairvoyance.service.TeamService;
import io.clairvoyance.service.UserService;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T09:57:49.374Z[GMT]")
@RestController
@Qualifier("UserApiController")
public class UserApiController implements UserApi {

    @Autowired
    private TeamService teamService;

    @Autowired
    private SportService sportService;

    @Autowired
    private UserService userService;

    @Autowired
    private BlogService blogService;

    private static final Logger log = LoggerFactory.getLogger(UserApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    public UserApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Void> removeSportInterest(@ApiParam(value = "The Sport Id", required = true) @PathVariable("sportId") Long sportId) {
        String accept = request.getHeader("Accept");
        User user = getUserFromRequest();
        try {
            user.removeInterestedInSportItem(sportService.getSportById(sportId));
            userService.saveUser(user);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (EntryNotFoundException e) {
            log.error("Could not get sport", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Void> removeTeamInterest(@ApiParam(value = "The Team Id", required = true) @PathVariable("teamId") Long teamId) {
        String accept = request.getHeader("Accept");
        User user = getUserFromRequest();
        try {
            user.removeInterestedInTeamItem(teamService.getTeamById(teamId));
            userService.saveUser(user);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (EntryNotFoundException e) {
            log.error("Could not get team", e);
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Void> sportInterest(@ApiParam(value = "The Sport Id", required = true) @PathVariable("sportId") Long sportId) {
        String accept = request.getHeader("Accept");
        User user = getUserFromRequest();
        try {
            user.addInterestedInSportItem(sportService.getSportById(sportId));
            userService.saveUser(user);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (EntryNotFoundException e) {
            log.error("Could not get sport", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Void> teamInterest(@ApiParam(value = "The Team Id", required = true) @PathVariable("teamId") Long teamId) {
        String accept = request.getHeader("Accept");
        User user = getUserFromRequest();
        try {
            user.addInterestedInTeamItem(teamService.getTeamById(teamId));
            userService.saveUser(user);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (EntryNotFoundException e) {
            log.error("Could not get team", e);
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<User> user() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                User user = getUserFromRequest();
                return new ResponseEntity<User>(user, HttpStatus.OK);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<User>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<User>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Team>> userTeams() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                User user = getUserFromRequest();
                return new ResponseEntity<List<Team>>(new ArrayList<>(user.getInterestedInTeam()), HttpStatus.OK);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Team>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Team>>(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<List<Blog>> userBlogs(@RequestParam(value = "simple", defaultValue = "false", required = false) Boolean simple,
                                                @RequestParam(value = "sort", defaultValue = "latest", required = false) String sort,
                                                @RequestParam(value = "sport", required = false) Long sportId,
                                                @RequestParam(value = "team", required = false) Long teamId,
                                                @RequestParam(value = "no-cover", defaultValue = "false", required = false) Boolean noCover,
                                                @RequestParam(value = "limit", defaultValue = "10", required = false) Long limit) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            Long userId = getUserFromRequest().getId();
            List<Blog> blogs = blogService.getBlogsByUserId(userId);

            if (sort.equals("alphabet")) {
                blogs.sort(Comparator.comparing(Blog::getTitle));
            } else {
                blogs.sort(Comparator.comparing(Blog::getDate));
            }

            blogs = blogs.stream()
                         .filter(blog -> sportId == null)
                         .filter(blog -> teamId == null)
                         .peek(blog -> {
                             if (simple) {
                                 blog.setContent("");
                                 blog.setDate(null);
                                 blog.setTags(null);
                             }
                         })
                         .peek(blog -> {
                             if (noCover) {
                                 blog.setCover("");
                             }
                         })
                         .limit(limit)
                         .collect(Collectors.toList());

            return ResponseEntity.ok(blogs);
        }
        return ResponseEntity.badRequest().build();
    }

    @Override
    public ResponseEntity<List<Long>> userRights() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                User user = getUserFromRequest();
                List<Long> leagueIds = user.getLeagues()
                                           .stream()
                                           .map(league -> league.getId())
                                           .collect(Collectors.toList());
                return new ResponseEntity<List<Long>>(leagueIds, HttpStatus.OK);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Long>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Long>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Sport>> userSports() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                User user = getUserFromRequest();
                return new ResponseEntity<List<Sport>>(new ArrayList<>(user.getInterestedInSport()), HttpStatus.OK);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Sport>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<List<Sport>>(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<Void> requestRight(RightRequest rightRequest) {

        rightRequest.sendEmail(userService.getUser());

        return ResponseEntity.ok().build();
    }

    private User getUserFromRequest() {
        return userService.getUser();
    }
}
