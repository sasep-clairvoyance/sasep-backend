package io.clairvoyance.api;

import io.clairvoyance.model.League;
import io.clairvoyance.model.Team;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Api(value = "leagues")
public interface LeaguesApi {

    @ApiOperation(value = "Get League info", nickname = "league", response = League.class, tags = {"guests", "users",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "League found", response = League.class),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 404, message = "League not found")})
    @RequestMapping(value = "/leagues/{leagueId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<League> league(@ApiParam(value = "The League ID", required = true) @PathVariable("leagueId") Long leagueId);

    @ApiOperation(value = "Returns all Leagues", nickname = "leagues", response = League.class, responseContainer = "List", tags = {"guests", "users",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Leagues found", response = League.class, responseContainer = "List")})
    @RequestMapping(value = "/leagues",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<League>> leagues();

    @ApiOperation(value = "Get teams by league", nickname = "teams", response = Team.class, responseContainer = "List", tags = {"guests", "users",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Teams found", response = Team.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad request")})
    @RequestMapping(value = "/leagues/{leagueId}/teams",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<Team>> teams(@ApiParam(value = "The League ID", required = true) @PathVariable("leagueId") Long leagueId);
}
