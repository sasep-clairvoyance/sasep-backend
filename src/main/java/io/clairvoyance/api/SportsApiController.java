package io.clairvoyance.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.clairvoyance.model.Match;
import io.clairvoyance.model.Player;
import io.clairvoyance.model.Sport;
import io.clairvoyance.model.Team;
import io.clairvoyance.repository.EntryNotFoundException;
import io.clairvoyance.service.SportService;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T09:57:49.374Z[GMT]")
@Controller
public class SportsApiController implements SportsApi {

    private static final Logger log = LoggerFactory.getLogger(SportsApiController.class);
    private final ObjectMapper objectMapper;
    private final HttpServletRequest request;
    @Autowired
    private SportService service;

    @Autowired
    public SportsApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<List<Match>> matches(@ApiParam(value = "The Sport ID", required = true) @PathVariable("sportId") Long sportId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            return ResponseEntity.ok(service.getMatchesBySportId(sportId));
        }

        return new ResponseEntity<List<Match>>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ResponseEntity<List<Player>> players(@ApiParam(value = "The Sport ID", required = true) @PathVariable("sportId") Long sportId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            return ResponseEntity.ok(service.getPlayersBySportId(sportId));
        }

        return new ResponseEntity<List<Player>>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ResponseEntity<Sport> sport(@ApiParam(value = "The Sport ID", required = true) @PathVariable("sportId") Long sportId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return ResponseEntity.ok(service.getSportById(sportId));
            } catch (EntryNotFoundException e) {
                log.error("Could not get sport", e);
                return new ResponseEntity<Sport>(HttpStatus.NOT_FOUND);
            }
        }

        return new ResponseEntity<Sport>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Sport>> sports() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            return ResponseEntity.ok(service.getAllSports());
        }

        return new ResponseEntity<List<Sport>>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ResponseEntity<List<Team>> teams(@ApiParam(value = "The Sport ID", required = true) @PathVariable("sportId") Long sportId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            return ResponseEntity.ok(service.getTeamsBySportId(sportId));
        }

        return new ResponseEntity<List<Team>>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
