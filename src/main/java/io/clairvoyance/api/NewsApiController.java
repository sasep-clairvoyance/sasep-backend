package io.clairvoyance.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.clairvoyance.model.Blog;
import io.clairvoyance.repository.EntryNotFoundException;
import io.clairvoyance.service.BlogService;
import io.clairvoyance.service.UserService;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class NewsApiController implements NewsApi {
    private static final Logger log = LoggerFactory.getLogger(NewsApiController.class);
    private final ObjectMapper objectMapper;
    private final HttpServletRequest request;
    @Autowired
    private BlogService blogService;

    @Autowired
    private UserService userService;

    @Autowired
    public NewsApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Override
    public ResponseEntity<List<Blog>> returnAllBlogArticles(@RequestParam(value = "simple", defaultValue = "false", required = false) Boolean simple,
                                                            @RequestParam(value = "sort", defaultValue = "latest", required = false) String sort,
                                                            @RequestParam(value = "sport", required = false) Long sportId,
                                                            @RequestParam(value = "team", required = false) Long teamId,
                                                            @RequestParam(value = "no-cover", defaultValue = "false", required = false) Boolean noCover,
                                                            @RequestParam(value = "only-published", defaultValue = "true", required = false) Boolean onlyPublished) {

        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                List<Blog> blogArticles = blogService.getAllBlogEntries();

                if (sort.equals("alphabet")) {
                    blogArticles.sort(Comparator.comparing(Blog::getTitle));
                } else {
                    blogArticles.sort(Comparator.comparing(Blog::getDate));
                }

                blogArticles = blogArticles.stream()
                                           .filter(blog -> sportId == null)
                                           .filter(blog -> teamId == null)
                                           .filter(blog -> !onlyPublished || blog.getPublished())
                                           .peek(blog -> {
                                               if (simple) {
                                                   blog.setContent("");
                                                   blog.setDate(null);
                                                   blog.setTags(null);
                                               }
                                           })
                                           .peek(blog -> {
                                               if (noCover) {
                                                   blog.setCover("");
                                               }
                                           })
                                           .collect(Collectors.toList());

                return new ResponseEntity<>(blogArticles, HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<Blog> returnBlogArticleById(Long blogId) {

        String accept = request.getHeader("Accept");

        if (accept != null && accept.contains("application/json")) {
            try {
                Blog blogArticle = blogService.getBlogArticleById(blogId);
                return new ResponseEntity<>(blogArticle, HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<Long> blog(@ApiParam @Valid @RequestBody Blog blog) {
        String accept = request.getHeader("Accept");
        if (accept != null) {
            if (blog.getPublished()) {
                blog.setDate(new Date());
            }
            blog = blogService.save(blog);
            return ResponseEntity.ok(blog.getId());
        }
        log.info("Faild save attempt. There is no user with the name " + blog.getAuthor());
        return ResponseEntity.badRequest().build();
    }

    @Override
    public ResponseEntity<Void> blogupdate(@ApiParam @RequestBody Long blogId, @ApiParam @Valid @RequestBody Blog blog) {
        String accept = request.getHeader("Accept");
        if (accept != null) {
            try {
                blogService.update(blogId, blog);
                return ResponseEntity.ok().build();
            } catch (EntryNotFoundException e) {
                log.info("Failed update attempt. There is no blog with the id: " + blogId, e);
                return ResponseEntity.badRequest().build();
            }
        }

        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
