package io.clairvoyance.api;

import io.clairvoyance.model.Match;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Api(value = "matches", description = "the matches API")
public interface MatchesApi {

    @ApiOperation(value = "Returns one match", nickname = "match", notes = "", response = Match.class, tags={ "users","guests", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Match found", response = Match.class),
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 404, message = "Match not found") })
    @RequestMapping(value = "/matches/{matchId}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<Match> match(@ApiParam(value = "The Match ID",required=true) @PathVariable("matchId") Long matchId);

}
