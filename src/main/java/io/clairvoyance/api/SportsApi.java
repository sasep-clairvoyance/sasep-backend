package io.clairvoyance.api;

import io.clairvoyance.model.Match;
import io.clairvoyance.model.Player;
import io.clairvoyance.model.Sport;
import io.clairvoyance.model.Team;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Api(value = "sports", description = "the sports API")
public interface SportsApi {

    @ApiOperation(value = "Returns all Matches of a Sport", nickname = "matches", notes = "", response = Match.class, responseContainer = "List", tags={ "users","guests", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Matches found", response = Match.class, responseContainer = "List"),
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 404, message = "Ressource not found") })
    @RequestMapping(value = "/sports/{sportId}/matches",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<List<Match>> matches(@ApiParam(value = "The Sport ID",required=true) @PathVariable("sportId") Long sportId);


    @ApiOperation(value = "Returns all Players of a Sport", nickname = "players", notes = "", response = Player.class, responseContainer = "List", tags={ "users","guests", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Player found", response = Player.class, responseContainer = "List"),
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 404, message = "Ressource not found") })
    @RequestMapping(value = "/sports/{sportId}/players",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<List<Player>> players(@ApiParam(value = "The Sport ID",required=true) @PathVariable("sportId") Long sportId);


    @ApiOperation(value = "Reutrns one Sport", nickname = "sport", notes = "", response = Sport.class, tags={ "guests","users", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Sport found", response = Sport.class),
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 404, message = "Sport not found") })
    @RequestMapping(value = "/sports/{sportId}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<Sport> sport(@ApiParam(value = "The Sport ID",required=true) @PathVariable("sportId") Long sportId);


    @ApiOperation(value = "Reutrns all Sports", nickname = "sports", notes = "", response = Sport.class, responseContainer = "List", tags={ "guests","users", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Sports found", response = Sport.class, responseContainer = "List") })
    @RequestMapping(value = "/sports",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<List<Sport>> sports();


    @ApiOperation(value = "Returns all Teams of a Sport", nickname = "teams", notes = "", response = Team.class, responseContainer = "List", tags={ "guests","users", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Teams found", response = Team.class, responseContainer = "List"),
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 404, message = "Ressource not found") })
    @RequestMapping(value = "/sports/{sportId}/teams",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<List<Team>> teams(@ApiParam(value = "The Sport ID",required=true) @PathVariable("sportId") Long sportId);

}
