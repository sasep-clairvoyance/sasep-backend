package io.clairvoyance.api;

import io.clairvoyance.model.Player;
import io.clairvoyance.model.Team;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Api(value = "teams")
public interface TeamsApi {

    @ApiOperation(value = "Returns all Teams", nickname = "teams", notes = "", response = Team.class, responseContainer = "List", tags={ "guests","users", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Teams found", response = Team.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Ressource not found") })
    @RequestMapping(value = "/teams",
            produces = { "application/json" },
            method = RequestMethod.GET)
    ResponseEntity<List<Team>> teams();

    @ApiOperation(value = "Returns one Team", nickname = "team", notes = "", response = Team.class, tags={ "guests","users", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Team found", response = Team.class),
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 404, message = "Team not found"),
        @ApiResponse(code = 200, message = "Default response for testing purpose", response = Team.class) })
    @RequestMapping(value = "/teams/{teamId}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<Team> team(@ApiParam(value = "The Team ID",required=true) @PathVariable("teamId") Long teamId);


    @ApiOperation(value = "Returns all players of a Team", nickname = "teamPlayers", notes = "", response = Player.class, responseContainer = "List", tags={ "guests","users", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Players found", response = Player.class, responseContainer = "List"),
        @ApiResponse(code = 400, message = "Bad request"),
        @ApiResponse(code = 404, message = "Ressource not found") })
    @RequestMapping(value = "/teams/{teamId}/players",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<List<Player>> teamPlayers(@ApiParam(value = "The Team Id",required=true) @PathVariable("teamId") Long teamId);


}
