package io.clairvoyance.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.clairvoyance.model.Match;
import io.clairvoyance.repository.EntryNotFoundException;
import io.clairvoyance.service.MatchService;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T09:57:49.374Z[GMT]")
@Controller
public class MatchesApiController implements MatchesApi {

    private static final Logger log = LoggerFactory.getLogger(MatchesApiController.class);
    private final ObjectMapper objectMapper;
    private final HttpServletRequest request;
    @Autowired
    private MatchService matchService;

    @Autowired
    public MatchesApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Match> match(@ApiParam(value = "The Match ID", required = true) @PathVariable("matchId") Long matchId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return ResponseEntity.ok(matchService.getMatchById(matchId));
            } catch (EntryNotFoundException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Match>(HttpStatus.BAD_REQUEST);
            }
        }

        return new ResponseEntity<Match>(HttpStatus.NOT_IMPLEMENTED);
    }

}
