package io.clairvoyance.api;

import io.clairvoyance.model.CompareBody;
import io.clairvoyance.model.CompareResponse;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Api(value = "compare", description = "the compare API")
public interface CompareApi {

    @ApiOperation(value = "Compares two teams by elo", nickname = "compareElo", notes = "", response = CompareResponse.class, tags = {"guests", "users",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Comparison succesfull", response = CompareResponse.class),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 404, message = "Ressource not found"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    @RequestMapping(value = "/compare/elo",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<CompareResponse> compareElo(@ApiParam(value = "") @Valid @RequestBody CompareBody body);

}
