package io.clairvoyance.api;

import io.clairvoyance.model.Player;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Api(value = "players", description = "the players API")
public interface PlayersApi {

    @ApiOperation(value = "Returns one Player", nickname = "player", notes = "", response = Player.class, tags={ "users","guests", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Player found", response = Player.class),
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 404, message = "Player not found") })
    @RequestMapping(value = "/players/{playerId}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<Player> player(@ApiParam(value = "The Player ID",required=true) @PathVariable("playerId") Long playerId);

}
