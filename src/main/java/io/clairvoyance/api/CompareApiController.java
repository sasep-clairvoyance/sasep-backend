package io.clairvoyance.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.clairvoyance.model.CompareBody;
import io.clairvoyance.model.CompareResponse;
import io.clairvoyance.repository.EntryNotFoundException;
import io.clairvoyance.service.CompareService;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T09:57:49.374Z[GMT]")
@Controller
public class CompareApiController implements CompareApi {
    private static final Logger log = LoggerFactory.getLogger(CompareApiController.class);
    private final ObjectMapper objectMapper;
    private final HttpServletRequest request;
    @Autowired
    private CompareService compareService;

    @Autowired
    public CompareApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<CompareResponse> compareElo(@ApiParam(value = "") @Valid @RequestBody CompareBody compareBody) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return ResponseEntity.ok(compareService.response(compareBody));
            } catch (EntryNotFoundException e) {
                log.error("Could not get team", e);
                return new ResponseEntity<CompareResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<CompareResponse>(HttpStatus.NOT_IMPLEMENTED);
    }

}
