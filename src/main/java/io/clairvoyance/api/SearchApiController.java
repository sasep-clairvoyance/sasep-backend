package io.clairvoyance.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.clairvoyance.model.SearchBody;
import io.clairvoyance.model.SearchResponse;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T09:57:49.374Z[GMT]")
@Controller
public class SearchApiController implements SearchApi {

    private static final Logger log = LoggerFactory.getLogger(SearchApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public SearchApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<SearchResponse> search(@ApiParam(value = "The search request") @Valid @RequestBody SearchBody body) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<SearchResponse>(objectMapper.readValue("{\n  \"sports\" : [ {\n    \"backColor\" : \"blue\",\n    \"imageUrl\" : \"/logos/sports/LoLlogo.png\",\n    \"name\" : \"League of Legends\",\n    \"id\" : 121512\n  } ]," +
                        "\n  \"teams\" : [ {\n    \"league\" : \"LCS\",\n    \"name\" : \"SKT Telekom T1\",\n    \"logo\" : \"/logos/logo.png\",\n    \"elo\" : 42,\n    \"id\" : 6\n  }, " +
                        "{\n  \"league\" : \"LCS\",\n  \"name\" : \"SKT Telekom T1\",\n  \"logo\" : \"/logos/SKTT1logo.png\",\n  \"elo\" : 42,\n  \"id\" : 1337\n}, " +
                        "{\n  \"league\" : \"LCS\",\n  \"name\" : \"Fnatic\",\n  \"logo\" : \"/logos/Fnaticlogo.png\",\n  \"elo\" : 20,\n  \"id\" : 420\n}, " +
                        "{\n  \"league\" : \"LCS\",\n  \"name\" : \"Samsung Galaxy\",\n  \"logo\" : \"/logos/SGlogo.png\",\n  \"elo\" : 35,\n  \"id\" : 58008\n}, " +
                        "{\n  \"league\" : \"LCS\",\n  \"name\" : \"Taipei Assassins\",\n  \"logo\" : \"/logos/TPAlogo.png\",\n  \"elo\" : 5,\n  \"id\" : 9001\n} ], " +
                        "\n  \"players\" : [ {\n  \"name\" : \"Enrique Cedeno \'xPeke\' Martinez\",\n  \"elo\" : 20,\n  \"id\" : 1\n}, " +
                        "{\n  \"name\" : \"Manuel \'LaMiaZeaLoT\' Mildenberger\",\n  \"elo\" : 20,\n  \"id\" : 2\n}, " +
                        "{\n  \"name\" : \"MacieJ \'Shushei\' Ratuszniak\",\n  \"elo\" : 20,\n  \"id\" : 3\n}, " +
                        "{\n  \"name\" : \"Lauri \'Cyanide\' Happonen\",\n  \"elo\" : 20,\n  \"id\" : 4\n}, " +
                        "{\n  \"name\" : \"Peter \'Melissan\' Meisrimel\",\n  \"elo\" : 20,\n  \"id\" : 5\n} ]\n}", SearchResponse.class), HttpStatus.OK);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<SearchResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<SearchResponse>(HttpStatus.NOT_IMPLEMENTED);
    }

}
