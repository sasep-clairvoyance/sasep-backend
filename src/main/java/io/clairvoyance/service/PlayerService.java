package io.clairvoyance.service;

import io.clairvoyance.model.Player;
import io.clairvoyance.repository.EntryNotFoundException;
import io.clairvoyance.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class PlayerService {

    @Autowired
    private PlayerRepository playerRepository;

    public Player getPlayerById(Long playerId) throws EntryNotFoundException {
        Optional<Player> player = playerRepository.findById(playerId);
        return player.orElseThrow(() -> new EntryNotFoundException("Could not find player with id: " + playerId));
    }
}
