package io.clairvoyance.service;

import io.clairvoyance.model.Blog;
import io.clairvoyance.model.Tag;
import io.clairvoyance.model.User;
import io.clairvoyance.repository.BlogRepository;
import io.clairvoyance.repository.EntryNotFoundException;
import io.clairvoyance.repository.TagRepositry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class BlogService {
    @Autowired
    private BlogRepository blogRepository;
    @Autowired
    private TagRepositry tagRepositry;
    @Autowired
    private UserService userService;

    public Blog save(Blog blog) {
        User user = userService.getUser();
        blog.setUser(user);
        List<Tag> blogTags = blog.getTags();
        blogTags.stream().filter(tag -> !tagRepositry.existsByName(tag.getName())).forEach(tag -> tagRepositry.save(tag));
        return blogRepository.save(blog);
    }

    public void update(Long blogId, Blog blog) throws EntryNotFoundException {
        Optional<Blog> old = blogRepository.findById(blogId);
        if (!old.isPresent()) {
            throw new EntryNotFoundException("This Id is not the blogpost your looking for");
        }
        if (!old.get().getPublished() && blog.getPublished()) {
            blog.setDate(new Date());
        }
        blog.setId(blogId);
        save(blog);
    }

    public List<Blog> getBlogsByUserId(Long userId) {
        return blogRepository.findAllByUserId(userId).collect(Collectors.toList());
    }

    public List<Blog> getAllBlogEntries() {
        List<Blog> blogs = new ArrayList<>();
        blogRepository.findAll().forEach(blogs::add);
        return blogs;
    }

    public Blog getBlogArticleById(Long blogId) throws EntryNotFoundException {
        return blogRepository.findById(blogId).orElseThrow(() -> new EntryNotFoundException("We do not store this blogpost " + blogId));
    }


}
