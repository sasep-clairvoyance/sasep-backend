package io.clairvoyance.service;

import io.clairvoyance.model.League;
import io.clairvoyance.model.Team;
import io.clairvoyance.repository.EntryNotFoundException;
import io.clairvoyance.repository.LeagueRepository;
import io.clairvoyance.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class LeagueService {
    @Autowired
    private LeagueRepository leagueRepository;
    @Autowired
    private TeamRepository teamRepository;

    public League getLeagueById(Long leagueId) throws Exception {
        return leagueRepository.findById(leagueId).orElseThrow(() -> new EntryNotFoundException("No league with the following id could be found: " + leagueId));
    }

    public List<League> getAllLeagues() {
        List<League> leagues = new ArrayList<>();
        leagueRepository.findAll().forEach(leagues::add);
        return leagues;
    }

    public List<Team> getTeamsById(Long leagueId) {
        return teamRepository.findDistinctByLeague_Id(leagueId).collect(Collectors.toList());
    }

}
