package io.clairvoyance.service;

import io.clairvoyance.model.Match;
import io.clairvoyance.repository.EntryNotFoundException;
import io.clairvoyance.repository.MatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class MatchService {
    @Autowired
    private MatchRepository matchRepository;

    public Match getMatchById(Long matchId) throws EntryNotFoundException {
        Optional<Match> match = matchRepository.findById(matchId);
        return match.orElseThrow(() -> new EntryNotFoundException("Could not find match with id: " + matchId));
    }
}
