package io.clairvoyance.service;

import io.clairvoyance.model.User;
import io.clairvoyance.repository.EntryNotFoundException;
import io.clairvoyance.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuthenticationService authenticationService;


    public User getUser() {
        User keycloakUser = authenticationService.getUser();
        sync();
        return userRepository.findByUsername(keycloakUser.getUsername()).orElse(keycloakUser);
    }

    public boolean saveUser(User user) {
        userRepository.save(user);
        return true;
    }

    public boolean sync() {
        User user = authenticationService.getUser();
        boolean present = userRepository.findByUsername(user.getUsername()).isPresent();
        if (!present) {
            userRepository.save(user);
        }
        return !present;
    }

    public User getUserByUsername(String username) throws EntryNotFoundException {
        Optional<User> user = userRepository.findByUsername(username);
        return user.orElseThrow(() -> new EntryNotFoundException("Could not find user: " + username));
    }

}
