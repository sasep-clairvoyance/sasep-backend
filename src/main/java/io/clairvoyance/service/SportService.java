package io.clairvoyance.service;

import io.clairvoyance.model.Match;
import io.clairvoyance.model.Player;
import io.clairvoyance.model.Sport;
import io.clairvoyance.model.Team;
import io.clairvoyance.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class SportService {
    @Autowired
    private SportRepository sportRepository;
    @Autowired
    private MatchRepository matchRepository;
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private TeamRepository teamRepository;

    public Sport getSportById(long sportId) throws EntryNotFoundException {
        Optional<Sport> sport = sportRepository.findById(sportId);
        return sport.orElseThrow(() -> new EntryNotFoundException("Could not find sport with id: " + sportId));
    }

    public List<Sport> getAllSports() {
        List<Sport> sports = new ArrayList<>();
        sportRepository.findAll().forEach(sports::add);
        return sports;
    }

    public List<Match> getMatchesBySportId(Long sportId) {
        return matchRepository.findDistinctByTeam1_League_Sport_Id(sportId).collect(Collectors.toList());
    }

    public List<Player> getPlayersBySportId(Long sportId) {
        return playerRepository.findDistinctByTeam_League_Sport_Id(sportId).collect(Collectors.toList());
    }

    public List<Team> getTeamsBySportId(Long sportId) {
        return teamRepository.findDistinctByLeague_Sport_Id(sportId).collect(Collectors.toList());
    }
}
