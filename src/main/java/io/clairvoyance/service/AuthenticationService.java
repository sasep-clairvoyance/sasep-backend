package io.clairvoyance.service;

import io.clairvoyance.model.User;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.representations.AccessToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {

    public String getUsername() {
        return getPrincipal().getName();
    }

    private KeycloakPrincipal getPrincipal() {
        return (KeycloakPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public User getUser() {
        AccessToken accessToken = getAccessToken();
        return new User(accessToken.getPreferredUsername(), accessToken.getEmail(), accessToken.getGivenName(), accessToken.getFamilyName());
    }

    public AccessToken getAccessToken() {
        return getPrincipal().getKeycloakSecurityContext().getToken();
    }
}
