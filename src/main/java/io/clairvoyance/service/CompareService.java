package io.clairvoyance.service;

import io.clairvoyance.model.CompareBody;
import io.clairvoyance.model.CompareResponse;
import io.clairvoyance.model.Team;
import io.clairvoyance.repository.EntryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class CompareService {
    @Autowired
    private TeamService teamService;

    public CompareResponse response(CompareBody request) throws EntryNotFoundException {
        Team team1 = teamService.getTeamById(request.getTeam1Id());
        Team team2 = teamService.getTeamById(request.getTeam2Id());

        CompareResponse response = new CompareResponse();
        response.compare(team1, team2);

        return response;
    }
}
