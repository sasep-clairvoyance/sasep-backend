package io.clairvoyance.service;

import io.clairvoyance.model.Player;
import io.clairvoyance.model.Team;
import io.clairvoyance.repository.EntryNotFoundException;
import io.clairvoyance.repository.PlayerRepository;
import io.clairvoyance.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private PlayerRepository playerRepository;


    public Team getTeamById(long teamId) throws EntryNotFoundException {
        Optional<Team> team = teamRepository.findById(teamId);
        return team.orElseThrow(() -> new EntryNotFoundException("Could not find team with id: " + teamId));
    }

    public List<Player> getPlayersByTeamId(Long teamId) {
        return playerRepository.findDistinctByTeam_Id(teamId).collect(Collectors.toList());
    }

    public List<Team> getAllTeams() {
        List<Team> teams = new ArrayList<>();
        teamRepository.findAll().forEach(teams::add);
        return teams;
    }
}
