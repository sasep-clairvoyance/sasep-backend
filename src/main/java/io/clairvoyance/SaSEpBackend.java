package io.clairvoyance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
// Ignore warning for using args without any extra check as they get passed to SpringBoot and we don't use any sensitive information as arguments
@SuppressWarnings("java:S4823")
public class SaSEpBackend {

    // start everything
    public static void main(String[] args) {
        SpringApplication.run(SaSEpBackend.class, args);
    }

}

