package io.clairvoyance.repository;

import io.clairvoyance.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByUsername(String username);

    User findById(long id);

    void deleteById(long id);
}
