package io.clairvoyance.repository;

import io.clairvoyance.model.Blog;
import org.springframework.data.repository.CrudRepository;

import java.util.stream.Stream;

public interface BlogRepository extends CrudRepository<Blog, Long> {
    Stream<Blog> findAllByUserId(Long userId);
}
