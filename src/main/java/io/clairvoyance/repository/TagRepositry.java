package io.clairvoyance.repository;

import io.clairvoyance.model.Tag;
import org.springframework.data.repository.CrudRepository;

public interface TagRepositry extends CrudRepository<Tag, Long> {
    boolean existsByName(String tag);
}
