package io.clairvoyance.repository;

import io.clairvoyance.model.Sport;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface SportRepository extends CrudRepository<Sport,Long> {
}
