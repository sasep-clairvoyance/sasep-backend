package io.clairvoyance.repository;

import io.clairvoyance.model.League;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface LeagueRepository extends CrudRepository<League, Long> {
}
