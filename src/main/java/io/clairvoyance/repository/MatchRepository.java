package io.clairvoyance.repository;

import io.clairvoyance.model.Match;
import org.springframework.data.repository.CrudRepository;

import java.util.stream.Stream;

public interface MatchRepository extends CrudRepository<Match, Long> {

    Stream<Match> findDistinctByTeam1_League_Sport_Id(Long sportId);
}
