package io.clairvoyance.repository;

import io.clairvoyance.model.Player;
import org.springframework.data.repository.CrudRepository;

import java.util.stream.Stream;

public interface PlayerRepository extends CrudRepository<Player, Long> {
    Stream<Player> findDistinctByTeam_Id(Long teamId);

    Stream<Player> findDistinctByTeam_League_Sport_Id(Long sportId);
}
