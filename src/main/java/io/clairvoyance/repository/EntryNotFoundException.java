package io.clairvoyance.repository;

public class EntryNotFoundException extends Exception {

    public EntryNotFoundException(String message) {
        super(message);
    }
}
