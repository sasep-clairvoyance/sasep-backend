package io.clairvoyance.repository;

import io.clairvoyance.model.Team;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.stream.Stream;

@Repository
@Transactional
public interface TeamRepository extends CrudRepository<Team, Long> {
    Stream<Team> findDistinctByLeague_Sport_Id(Long sportId);

    Team getTeamById(Long teamid);

    Stream<Team> findDistinctByLeague_Id(Long leagueId);
}
