package io.clairvoyance.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hibernate.validator.internal.util.CollectionHelper.newArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${config.oauth2.accessTokenUri}")
    private String accessTokenUri;

    @Value("${config.oauth2.client.id}")
    private String clientId;

    @Value("${config.oauth2.client.secret}")
    private String clientSecret;

    @Value("${config.oauth2.userAuthorizationUri}")
    private String authorizationUri;

    @Value("${config.oauth2.realm}")
    private String realm;

    /**
     * @return Docket
     */
    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any()).paths(PathSelectors.any()).build()
                .securityContexts(Collections.singletonList(securityContext())).securitySchemes(Arrays.asList(securitySchema())).apiInfo(apiInfo());

    }

    private OAuth securitySchema() {

        List<AuthorizationScope> authorizationScopeList = newArrayList();
        authorizationScopeList.add(new AuthorizationScope("openid", "openid"));

        List<GrantType> grantTypes = newArrayList();
        GrantType standardFlow = new AuthorizationCodeGrant(new TokenRequestEndpoint(authorizationUri, clientId, clientSecret), new TokenEndpoint(accessTokenUri, "bearer"));
        grantTypes.add(standardFlow);

        return new OAuth("oauth2", authorizationScopeList, grantTypes);
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(defaultAuth()).build();
    }

    private List<SecurityReference> defaultAuth() {

        final AuthorizationScope[] authorizationScopes = new AuthorizationScope[3];
        authorizationScopes[0] = new AuthorizationScope("read", "read all");
        authorizationScopes[1] = new AuthorizationScope("trust", "trust all");
        authorizationScopes[2] = new AuthorizationScope("write", "write all");

        return Collections.singletonList(new SecurityReference("oauth2", authorizationScopes));
    }

    @Bean
    public SecurityConfiguration security() {
        return new SecurityConfiguration(clientId, clientSecret, realm, clientId, "Bearer access token", null, false);
    }

    /**
     * @return ApiInf
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("SaSEp: Clairvoyance").description("Swagger API").termsOfServiceUrl("https://sweb.mysticrunes.net")
                .contact(new Contact("Developers", "https://sweb.mysticrunes.net", "")).license("Open Source").version("1.0.0").build();

    }

}

