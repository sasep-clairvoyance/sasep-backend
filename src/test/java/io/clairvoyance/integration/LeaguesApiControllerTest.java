package io.clairvoyance.integration;

import io.clairvoyance.service.AuthenticationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "/application-integrationtest.properties")
@Sql(scripts = "/test-data.sql", config= @SqlConfig(errorMode = SqlConfig.ErrorMode.CONTINUE_ON_ERROR))
public class LeaguesApiControllerTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @MockBean
    public AuthenticationService authenticationService;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void givenRequestAllLeagues_shouldReturn2Entries() throws Exception {
        mvc.perform(get("/leagues").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$.length()").value(3));
    }

    @Test
    public void givenRequestLeagueWithValidId_shouldReturnLeague() throws Exception {
        mvc.perform(get("/leagues/10").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$.id").value("10"))
           .andExpect(jsonPath("$.name").value("League Championship Series"));
    }

    @Test
    public void givenRequestLeagueWithInvalidId_shouldReturnError() throws Exception {
        mvc.perform(get("/leagues/42").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isNotFound());
    }

    @Test
    public void givenRequestLeagueTeamsWithValidId_shouldReturnTeams() throws Exception {
        mvc.perform(get("/leagues/10/teams").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$.length()").value(4));
    }

    @Test
    public void givenRequestLeagueTeamsWithInvalidId_shouldReturnError() throws Exception {
        mvc.perform(get("/leagues/42/teams").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isNotFound());
    }
}