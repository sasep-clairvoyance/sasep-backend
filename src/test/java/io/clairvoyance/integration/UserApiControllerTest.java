package io.clairvoyance.integration;

import io.clairvoyance.model.User;
import io.clairvoyance.service.AuthenticationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "/application-integrationtest.properties")
@Sql(scripts = "/test-data.sql", config = @SqlConfig(errorMode = SqlConfig.ErrorMode.CONTINUE_ON_ERROR))
public class UserApiControllerTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @MockBean
    public AuthenticationService authenticationService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        Mockito.when(authenticationService.getUser()).thenReturn(new User("test2", "test@test.com", "test", "test"));

        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void givenAuthRequestOnPrivateService_shouldSucceedWith200() throws Exception {
        mvc.perform(get("/user").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$.username").value("test2"));
    }

    @Test
    public void givenAuthRequestOnUserSports_shouldReturnEmptySportsList() throws Exception {
        mvc.perform(get("/user/sports").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$").isArray())
           .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    public void givenAuthRequestOnUserTeams_shouldReturnEmptyTeamsList() throws Exception {
        mvc.perform(get("/user/sports").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$").isArray())
           .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    public void givenAuthRequestOnRights_shouldReturnEmptyList() throws Exception {
        mvc.perform(get("/user/rights/leagues").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$").isArray())
           .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    public void addAndRemoveSportInterests() throws Exception {
        mvc.perform(get("/user/sports").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$").isArray())
           .andExpect(jsonPath("$").isEmpty());

        mvc.perform(put("/user/sports/1").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk());

        mvc.perform(get("/user/sports").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$").isArray())
           .andExpect(jsonPath("$[0].id").value("1"))
           .andExpect(jsonPath("$[0].name").value("League of Legends"));

        mvc.perform(delete("/user/sports/1").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk());

        mvc.perform(get("/user/sports").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$").isArray())
           .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    public void addAndRemoveTeamInterests() throws Exception {
        mvc.perform(get("/user/teams").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$").isArray())
           .andExpect(jsonPath("$").isEmpty());

        mvc.perform(put("/user/teams/100").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk());

        mvc.perform(get("/user/teams").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$").isArray())
           .andExpect(jsonPath("$[0].id").value("100"))
           .andExpect(jsonPath("$[0].name").value("Cloud9"));

        mvc.perform(delete("/user/teams/100").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk());

        mvc.perform(get("/user/teams").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$").isArray())
           .andExpect(jsonPath("$").isEmpty());
    }

}