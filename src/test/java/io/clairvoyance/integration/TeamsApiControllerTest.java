package io.clairvoyance.integration;

import io.clairvoyance.service.AuthenticationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "/application-integrationtest.properties")
@Sql(scripts = "/test-data.sql", config= @SqlConfig(errorMode = SqlConfig.ErrorMode.CONTINUE_ON_ERROR))
public class TeamsApiControllerTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @MockBean
    public AuthenticationService authenticationService;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void givenRequestAllTeams_shouldReturn2Entries() throws Exception {
        mvc.perform(get("/teams").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$.length()").value(4));
    }

    @Test
    public void givenRequestTeamWithValidId_shouldReturnTeam() throws Exception {
        mvc.perform(get("/teams/100").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$.id").value("100"))
           .andExpect(jsonPath("$.name").value("Cloud9"));
    }

    @Test
    public void givenRequestTeamWithInvalidId_shouldReturnError() throws Exception {
        mvc.perform(get("/teams/42").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isNotFound());
    }

    @Test
    public void givenRequestTeamPlayersWithValidId_shouldReturnPlayer() throws Exception {
        mvc.perform(get("/teams/100/players").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$.length()").value(5));
    }

    @Test
    public void givenRequestTeamPlayersWithInvalidId_shouldReturnError() throws Exception {
        mvc.perform(get("/teams/42/players").accept(MediaType.APPLICATION_JSON))
           .andExpect(status().isNotFound());
    }
}