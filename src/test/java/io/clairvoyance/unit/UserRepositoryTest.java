package io.clairvoyance.unit;

import io.clairvoyance.model.User;
import io.clairvoyance.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void whenFindByName_thenReturnUser() {
        // given
        User alex = new User("alex", "alex@alex.de", "Alex", "Alex");
        entityManager.persist(alex);
        entityManager.flush();

        // when
        Optional<User> found = userRepository.findByUsername(alex.getUsername());

        // then
        assertThat(found.isPresent()).isTrue();
        assertThat(found.get().getUsername()).isEqualTo(alex.getUsername());
    }

}