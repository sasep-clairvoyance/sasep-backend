package io.clairvoyance.unit;


import io.clairvoyance.model.Team;
import io.clairvoyance.repository.EntryNotFoundException;
import io.clairvoyance.repository.TeamRepository;
import io.clairvoyance.service.TeamService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

@RunWith(SpringRunner.class)
public class TeamServiceTest {

    @InjectMocks
    private TeamService teamService;

    @MockBean
    private TeamRepository teamRepository;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);

        Team team = new Team();
        team.name("test");
        team.id(1L);

        Mockito.when(teamRepository.findById(1L)).thenReturn(java.util.Optional.of(team));
    }

    @Test
    public void ifIdIsViable_TeamShouldBeFound() throws EntryNotFoundException {
        assertThat(teamService.getTeamById(1L).getName()).isEqualTo("test");
    }
}
