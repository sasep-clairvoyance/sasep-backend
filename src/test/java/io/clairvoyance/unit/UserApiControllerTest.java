package io.clairvoyance.unit;

import io.clairvoyance.model.User;
import io.clairvoyance.repository.EntryNotFoundException;
import io.clairvoyance.service.UserService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserApiControllerTest {

    User alex = new User("alex", "alex@alex.de", "Alex", "Alex");
    @Autowired
    private MockMvc mvc;
    @MockBean
    private UserService userService;


    @Before
    public void setUp() throws EntryNotFoundException {


        Mockito.when(userService.getUserByUsername("alex")).thenReturn(alex);

    }
    @Ignore
    @Test
    public void givenUser_whenGetUser_thenReturnJsonArray()
            throws Exception {

        this.mvc.perform(get("/user")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is("alex")));
    }
}