package io.clairvoyance.unit;


import io.clairvoyance.model.League;
import io.clairvoyance.repository.LeagueRepository;
import io.clairvoyance.service.LeagueService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class LeagueServiceTest {

    @InjectMocks
    private LeagueService leagueService;

    @MockBean
    private LeagueRepository leagueRepository;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        League league1 = new League(1l,"League 1", "1.png", false, "", new ArrayList<>());
        League league2 = new League(2l, "League 2", "2.jpg", false, "", new ArrayList<>());
        ArrayList<League> list = new ArrayList<League>();
        list.add(league1);
        list.add(league2);


        Mockito.when(leagueRepository.findById(1l)).thenReturn(Optional.of(league1));

        Mockito.when(leagueRepository.findAll()).thenReturn(list);

        Mockito.when(leagueRepository.existsById(1l)).thenReturn(true);
        Mockito.when(leagueRepository.existsById(2l)).thenReturn(true);
        Mockito.when(leagueRepository.existsById(3l)).thenReturn(false);
    }

    @Test
    public void whenIdValid_thenLeagueShouldBeFound() throws Exception{
        long id = 1l;
        League found = leagueService.getLeagueById(id);
        assertThat(found.getId()).isEqualTo(id);
        assertThat(found.getName()).isEqualTo("League 1");
        assertThat(found.getLogo()).isEqualTo("1.png");
    }

    @Test
    public void allTeamsShouldBeFound(){
        List<League> found = leagueService.getAllLeagues();
        League one = found.get(0);
        League two = found.get(1);

        assertThat(one.getId()).isEqualTo(1l);
        assertThat(two.getId()).isEqualTo(2l);

        assertThat(one.getName()).isEqualTo("League 1");
        assertThat(two.getName()).isEqualTo("League 2");

        assertThat(one.getLogo()).isEqualTo("1.png");
        assertThat(two.getLogo()).isEqualTo("2.jpg");
    }
}
