package io.clairvoyance.unit;

import io.clairvoyance.model.User;
import io.clairvoyance.repository.EntryNotFoundException;
import io.clairvoyance.service.AuthenticationService;
import io.clairvoyance.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class UserServiceTest {

    @Mock
    private UserService userService;

    @Mock
    private AuthenticationService authenticationService;

    @Before
    public void setUp() throws EntryNotFoundException {
        User alex = new User("alex", "alex@alex.de", "Alex", "Alex");

        Mockito.when(userService.getUserByUsername(alex.getUsername())).thenReturn(alex);
        Mockito.when(authenticationService.getUser()).thenReturn(alex);
        Mockito.when(authenticationService.getUsername()).thenReturn(alex.getUsername());

    }

    @Test
    public void whenValidUsername_thenUserShouldBeFound() throws EntryNotFoundException {
        String name = "alex";
        User found = userService.getUserByUsername(name);

        assertThat(found.getUsername())
                .isEqualTo(name);
    }
}