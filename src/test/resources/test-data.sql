insert into sport (id, name, thumbnail_url, back_color, image_url)
values (01, 'League of Legends', '/logos/sports/LoLthumb.png', 'blue', '/logos/sports/LoLlogo.png');
insert into sport (id, name, thumbnail_url, back_color, image_url)
values (02, 'Dota2', '/logos/sports/Dota2thumb.png', 'blue', '/logos/sports/Dota2logo.png');
insert into league (id, name, logo, privacy, owner, sport_id)
values (010, 'League Championship Series', '/logos/leagues/LCSlogo.png', false, null, 01);
insert into league (id, name, logo, privacy, owner, sport_id)
values (011, 'European Masters', '/logos/leagues/EUMlogo.png', true, 'Mikkel', 01);
insert into league (id, name, logo, privacy, owner, sport_id)
values (020, 'Esportal Dota 2 League', '/logos/leagues/ED2Llogo.png', false, 'Michael', 02);
insert into team (id, name, logo, elo, league_id)
values (0100, 'Cloud9', '/logos/Cloud9logo.png', 2644, 010);
insert into team (id, name, logo, elo, league_id)
values (0101, 'Evil Geniuses', '/logos/EGlogo.png', 2514, 010);
insert into team (id, name, logo, elo, league_id)
values (0102, '100 Thieves', '/logos/100Thieveslogo.png', 2412, 010);
insert into team (id, name, logo, elo, league_id)
values (0103, 'FlyQuest', '/logos/FlyQuestlogo.png', 2410, 010);
insert into match (id, date, location, team1_id, team2_id, winner_id)
values (01000, '2020-05-12', 'Atlanta', 0100, 0101, 0100);
insert into match (id, date, location, team1_id, team2_id, winner_id)
values (01001, '2020-05-12', 'Atlanta', 0100, 0102, 0102);
insert into match (id, date, location, team1_id, team2_id, winner_id)
values (01002, '2020-05-12', 'Atlanta', 0100, 0103, 0103);
insert into match (id, date, location, team1_id, team2_id, winner_id)
values (01003, '2020-05-12', 'Atlanta', 0101, 0102, 0101);
insert into match (id, date, location, team1_id, team2_id, winner_id)
values (01004, '2020-05-12', 'Atlanta', 0101, 0103, 0103);
insert into match (id, date, location, team1_id, team2_id, winner_id)
values (01005, '2020-05-12', 'Atlanta', 0102, 0103, 0103);
insert into player(id, name, elo, team_id)
values (010000, 'Eric Ritchie', 2631, 0100);
insert into player(id, name, elo, team_id)
values (010001, 'Robert Huang', 2311, 0100);
insert into player(id, name, elo, team_id)
values (010002, 'Yasin Dinçer', 2422, 0100);
insert into player(id, name, elo, team_id)
values (010003, 'Jesper Svenningsen', 2534, 0100);
insert into player(id, name, elo, team_id)
values (010004, 'Philippe Laflamme', 2467, 0100);
insert into player(id, name, elo, team_id)
values (010005, 'Heo Seung-hoon', 2654, 0101);
insert into player(id, name, elo, team_id)
values (010006, 'Dennis Johnsen', 2611, 0101);
insert into player(id, name, elo, team_id)
values (010007, 'Daniele di Mauro', 22231, 0101);
insert into player(id, name, elo, team_id)
values (010008, 'Bae Jun-sik', 2298, 0101);
insert into player(id, name, elo, team_id)
values (010009, 'Tristan Stidam', 2389, 0101);
insert into "user" (id, email, first_name, last_name, username)
values (1, 'test@test.com', 'test', 'test', 'test2');
insert into "user" (id, email, first_name, last_name, username)
values (2, 'test@test.com', 'test', 'test', 'test');